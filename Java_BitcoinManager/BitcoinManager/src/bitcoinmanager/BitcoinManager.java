/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package bitcoinmanager;

import java.util.*;
import java.io.*;

/**
 * This class proyect is a "Bitcoin Manager", a program that can be used to
 * analyze the quotes of the Bitcoin with .csv files and the "management of
 * wallets". <br>
 * When the program starts, it will create all the necessary directories for the
 * program to work. Then, it will check if there is a .csv at the "Quotes"
 * directory. Three thing can happen: <br> <br>
 * - If there isn't any quotes file, it will tell the user that a quotes file
 * needs to be at the directory to be loaded. Then, it will ask the user to
 * introduce a quotes file inside the "Quotes" directory and, after he
 * introduced the file, it needs to press "Enter". Then, the program will check
 * again for files. <br>
 * - If there is a single quotes file, it will load the only file that is
 * available. <br>
 * - If there is more than one quotes file, it will list them and will let the
 * user to choose between them. When the user has made his choice, it will load
 * the quotes file selected. <br> <br>
 * When the quotes file is loaded, it will let the user to perform different
 * actions by "commands". There are three categories: <br> <br>
 * - GENERAL COMMANDS <br> <br>
 * This commands are generic commands that will perform basic actions. The
 * following commands belong to this category: <br> <br>
 * - help: displays information about all the available commands. If you add any
 * available command after "help", it will display the specific information
 * about the command. <br>
 * - clear: clears the console. It only works if you are running the program
 * outside of a IDE. <br>
 * - quit: exits the program. <br> <br>
 * - ANALYSIS COMMANDS <br> <br>
 * This commands are used to perform mathematical calculations that are used to
 * analyze the quotes of the Bitcoin over time. The following commands belong to
 * this category: <br> <br>
 * - value: checks the value of the specified field of the quotes file as long
 * as the date is recorded in the file. <br>
 * - avg: checks the arithmetic average of the specified field of the quotes
 * file between two specified dates as long as both dates are recorded in the
 * file. <br>
 * - rent: checks the percentage of rentability between two specified dates as
 * long as both dates are recorded in the file. <br>
 * - monkeys: using the "monkey theory", displays the percentage of earning
 * money by "using blindfolded monkeys throwing darts at investing data". <br>
 * <br>
 * - WALLETS MANAGEMENT COMMANDS <br> <br>
 * This commands are used to create "wallets" and "perform transactions" with
 * them. The following commands belong to this category: <br> <br>
 * - list: list all the available wallets that are in the "Wallets" directory.
 * <br>
 * - new: creates a new wallet at the "Wallets" directory and selects it. <br>
 * - select: selects the wallet that the user specified as long as is available
 * at the "Wallets" directory. <br>
 * - buy: creates a "purchase transaction" and records it in the wallet,
 * converting the amount of USD introduced by the user to Bitcoin. <br>
 * - sell: creates a "sale transaction" and records it in the wallet, converting
 * the amoung of Bitcoin introduced by the user to USD as long as the wallet
 * holds the specified amount of Bitcoin. <br>
 * - balance: displays information about the wallet, like all the USD that the
 * user spent or earned with the wallet or the rentability.
 *
 * @author David Ruiz Agustín
 */
public class BitcoinManager {

    /**
     * Scanner that will be used to interact with the user.
     */
    public static Scanner userValues = new Scanner(System.in);
    /**
     * String that will store the user's directory.
     */
    public static String currentUserHomeDir = System.getProperty("user.home");
    /**
     * Variable that will define the location of the root of the directories
     * structure.
     */
    public static File baseDir = new File(currentUserHomeDir + File.separator + "BitcoinManager");
    /**
     * Variable that will define the location of the quotes directory.
     */
    public static File quotesDir = new File(baseDir.getAbsolutePath() + File.separator + "Quotes");
    /**
     * Variable that stores the quotes file that is going to be used.
     */
    public static File quotesFile = null;
    /**
     * Matrix that will store all the records from the quotes file.
     */
    public static String[][] quotesRecords = null;
    /**
     * Variable that will define the location of the wallets directory.
     */
    public static File walletsDir = new File(baseDir.getAbsolutePath() + File.separator + "Wallets");
    /**
     * Variable that stores the wallet file that is loaded.
     */
    public static File walletFile = null;
    /**
     * Variable that stores the position of the date when the last transaction
     * was made.
     */
    public static int walletLastDatePosition = 0;
    /**
     * Variable that stores the amount of bitcoin that the selected wallet has.
     */
    public static double totalBTCAmount = 0.0;

    /**
     * Starting point
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean quitProgram = false;
        if (createDirectories()) {

            loadQuotesFile();

            while (!quitProgram) {

                String[] commandSplitted = commandPrompt().split(" ");

                switch (commandSplitted[0].toLowerCase()) {
                    case "help":

                        if (commandSplitted.length > 1) {

                            switch (commandSplitted[1]) {

                                case "help":
                                    System.out.print("\nHELP - Displays a help menu with all the available commands. Can display more specific info about a command.\n");
                                    System.out.print("FORMAT: help <command>\n\n");
                                    break;
                                case "clear":
                                    System.out.print("\nCLEAR - Clears the screen. This command only works if you are executing the program outside a IDE.\n");
                                    System.out.print("FORMAT: clear\n\n");
                                    break;
                                case "quit":
                                    System.out.print("\nQUIT - Closes the program.\n");
                                    System.out.print("FORMAT: quit\n\n");
                                    break;
                                case "value":
                                    System.out.print("\nVALUE - Displays the value of the field at the selected date.\n");
                                    System.out.print("FORMAT: value date field\n\n");
                                    break;
                                case "avg":
                                    System.out.print("\nAVG - Displays the arithmetic average of the values of the introduced field between the initial date and final date.\n");
                                    System.out.print("FORMAT: avg initial_date final_date field\n\n");
                                    break;
                                case "rent":
                                    System.out.print("\nRENT - Displays the rentability between the buying date and the selling date.\n");
                                    System.out.print("FORMAT: rent buy_date sell_date\n\n");
                                    break;
                                case "monkeys":
                                    System.out.print("\nMONKEYS - Displays a estimation of the probability of obtaining a positive rentability by using the \"monkey theory\" created by Gordon Malkiel.\n");
                                    System.out.print("FORMAT: monkeys number_of_monkeys\n\n");
                                    break;
                                case "list":
                                    System.out.print("\nLIST - Displays a list with all the available wallets.\n\n");
                                    System.out.print("FORMAT: list\n\n");
                                    break;
                                case "new":
                                    System.out.print("\nNEW - Creates a new wallet with the introduced name and selects it automatically.\n");
                                    System.out.print("FORMAT: new wallet_name\n\n");
                                    break;
                                case "select":
                                    System.out.print("\nSELECT - Selects the wallet that is going to be managed.\n");
                                    System.out.print("FORMAT: select wallet_name\n\n");
                                    break;
                                case "buy":
                                    System.out.print("\nBUY - Registers a buy order of bitcoins in the introduced date. You must have a wallet selected beforehand to execute this command.\n");
                                    System.out.print("FORMAT: buy date USD_quantity\n\n");
                                    break;
                                case "sell":
                                    System.out.print("\nSELL - Registers a sell order of bitcoins in the introduced date. You must have a wallet selected beforehand to execute this command.\n");
                                    System.out.print("FORMAT: sell date BTC_quantity\n\n");
                                    break;
                                case "balance":
                                    System.out.print("\nBALANCE - Shows all the information about the wallet. You must have a wallet selected beforehand to execute this command.\n");
                                    System.out.print("FORMAT: balance\n\n");
                                    break;
                                default:
                                    System.out.print("\nERROR: The introduced command does not exist.\n\n");
                                    break;

                            }

                        } else {

                            System.out.print("\nGENERAL COMMANDS\n");
                            System.out.print("HELP - Displays a help menu with all the available commands. Can display more specific info about the command if you type \"help <command>\".\n");
                            System.out.print("CLEAR - Clears the screen. This command only works if you are executing the program outside a IDE.\n");
                            System.out.print("QUIT - Closes the program.\n\n");
                            System.out.print("QUOTES ANALYSIS COMMANDS\n");
                            System.out.print("VALUE - Displays the value of the field at the selected date.\n");
                            System.out.print("AVG - Displays the arithmetic average of the values of the introduced field between the initial date and final date.\n");
                            System.out.print("RENT - Displays the rentability between the buying date and the selling date.\n");
                            System.out.print("MONKEYS - Displays a estimation of the probability of obtaining a positive rentability by using the \"monkey theory\" created by Gordon Malkiel.\n\n");
                            System.out.print("WALLET MANAGEMENT COMMANDS\n");
                            System.out.print("LIST - Displays a list with all the available wallets.\n");
                            System.out.print("NEW - Creates a new wallet with the introduced name and selects it automatically.\n");
                            System.out.print("SELECT - Selects the wallet that is going to be managed.\n");
                            System.out.print("BUY - Registers a buy order of bitcoins in the introduced date. You must have a wallet selected beforehand to execute this command.\n");
                            System.out.print("SELL - Registers a sell order of bitcoins in the introduced date. You must have a wallet selected beforehand to execute this command.\n");
                            System.out.print("BALANCE - Shows all the information about the wallet. You must have a wallet selected beforehand to execute this command.\n\n");

                        }

                        break;
                    case "clear":
                        System.out.println();
                        clearScreenAndPrint();
                        break;
                    case "quit":
                        quitProgram = true;
                        break;
                    case "value":

                        if (commandSplitted.length == 3) {

                            System.out.print("\n" + checkValue(commandSplitted[1], commandSplitted[2].toLowerCase()) + "\n\n");

                        } else {

                            System.out.print("\nERROR: Invalid format for \"value\" command. Example: value 2021-12-09 open\n\n");

                        }

                        break;
                    case "avg":

                        if (commandSplitted.length == 4) {

                            System.out.print("\n" + checkAverage(commandSplitted[1], commandSplitted[2], commandSplitted[3].toLowerCase()) + "\n\n");

                        } else {

                            System.out.print("\nERROR: Invalid format for \"avg\" command. Example: avg 2021-12-09 2021-12-12 open\n\n");

                        }

                        break;
                    case "rent":

                        if (commandSplitted.length == 3) {

                            System.out.print("\n" + calculateRent(commandSplitted[1], commandSplitted[2]) + "\n\n");

                        } else {

                            System.out.print("\nERROR: Invalid format for \"rent\" command. Example: rent 2021-12-09 2021-12-12\n\n");

                        }

                        break;
                    case "monkeys":

                        if (commandSplitted.length == 2) {

                            try {

                                System.out.print("\n" + applyMonkeyTheory(Integer.valueOf(commandSplitted[1])) + "\n\n");

                            } catch (NumberFormatException e) {

                                System.out.print("\nERROR: You introduced a character that isn't a number or you have introduced too many monkeys.\n\n");

                            }

                        } else {

                            System.out.print("\nERROR: Invalid format for \"monkeys\" command. Example: monkeys 10\n\n");

                        }

                        break;
                    case "list":
                        listWallets();
                        break;
                    case "new":

                        if (commandSplitted.length == 2) {

                            System.out.print("\n" + newWallet(commandSplitted[1]) + "\n\n");

                        } else {

                            System.out.print("\nERROR: Invalid format for \"new\" command. Example: select \"wallet_name\"\n\n");

                        }

                        break;
                    case "select":

                        if (commandSplitted.length == 2) {

                            System.out.print("\n" + selectWallet(commandSplitted[1].toLowerCase()) + "\n\n");

                        } else {

                            System.out.print("\nERROR: Invalid format for \"select\" command. Example: select \"wallet_name\"\n\n");

                        }

                        break;

                    case "buy":

                        if (commandSplitted.length == 3) {

                            if (walletFile != null) {

                                try {

                                    System.out.print("\n" + buyBTC(commandSplitted[1], Double.valueOf(commandSplitted[2])) + "\n\n");

                                } catch (NumberFormatException e) {

                                    System.out.print("\nERROR: You introduced a invalid character into the USD amount.\n\n");

                                }

                            } else {

                                System.out.print("\nERROR: There isn't a selected wallet. Select a new one with the \"select\" command or create a new one with the \"new\" command.\n\n");

                            }

                        } else {

                            System.out.print("\nERROR: Invalid format for \"buy\" command. Example: buy 2023-11-20 10000\n\n");

                        }

                        break;

                    case "sell":

                        if (commandSplitted.length == 3) {

                            if (walletFile != null) {

                                try {

                                    System.out.print("\n" + sellBTC(commandSplitted[1], Double.valueOf(commandSplitted[2])) + "\n\n");

                                } catch (NumberFormatException e) {

                                    System.out.print("\nERROR: You introduced a invalid character into the BTC amount.\n\n");

                                }

                            } else {

                                System.out.print("\nERROR: There isn't a selected wallet. Select a new one with the \"select\" command or create a new one with the \"new\" command.\n\n");

                            }

                        } else {

                            System.out.print("\nERROR: Invalid format for \"sell\" command. Example: sell 2024-01-01 2\n\n");

                        }

                        break;

                    case "balance":

                        if (commandSplitted.length == 1) {

                            if (walletFile != null) {

                                walletBalance();

                            } else {

                                System.out.print("\nERROR: There isn't a selected wallet. Select a new one with the \"select\" command or create a new one with the \"new\" command.\n\n");

                            }

                        } else {

                            System.out.print("\nERROR: Invalid format for \"balance\" command. Example: balance\n\n");

                        }

                        break;

                    default:
                        System.out.print("\nERROR: Invalid command. Type \"help\" to check the available commands.\n\n");
                        break;
                }

            }

        }

    }

    /**
     * Method that will clear the user's screen if he is using Windows or Unix
     * OS. It will also print a "header" with the program name.
     */
    public static void clearScreenAndPrint() {

        try {

            final String currentOS = System.getProperty("os.name");

            if (currentOS.contains("Windows")) {

                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();

            } else {

                new ProcessBuilder("clear").inheritIO().start().waitFor();

            }

        } catch (IOException | InterruptedException e) {

            System.out.print("ERROR: Couldn't clear the screen.\n\n");

        }

        System.out.print("Bitcoin Manager by David Ruiz Agustín v1.0\n\n");

    }

    /**
     * Attempts to generate the directories structure.
     *
     * @return if it generated the directories successfully.
     */
    public static boolean createDirectories() {

        try {

            baseDir.mkdir();
            quotesDir.mkdir();
            walletsDir.mkdir();

        } catch (Exception e) {

            System.out.print("ERROR: Unexpected error while creating program directories.\n\n");
            return false;

        }

        return true;

    }

    /**
     * Loads all the quotes file that are available at the quotes files
     * directory. If the directory has more than one file, it will allow the
     * user to load the one that he prefers. If it's empty, it will ask the user
     * to introduce a quotes file in the quotes file directory.
     */
    public static void loadQuotesFile() {

        boolean filesLoaded = false;
        File[] quotesFilesListUnfiltered = null;

        while (!filesLoaded) {

            clearScreenAndPrint();

            quotesFilesListUnfiltered = quotesDir.listFiles();
            int filter = 0;

            for (int i = 0; i < quotesFilesListUnfiltered.length; i++) {

                if (quotesFilesListUnfiltered[i].getName().endsWith(".csv")) {

                    filter++;

                }

            }

            File[] quotesFilesListFiltered = new File[filter];
            filter = 0;

            for (int i = 0; i < quotesFilesListUnfiltered.length; i++) {

                if (quotesFilesListUnfiltered[i].getName().endsWith(".csv")) {

                    quotesFilesListFiltered[filter] = quotesFilesListUnfiltered[i];
                    filter++;

                }

            }

            if (quotesFilesListFiltered.length > 0) {

                if (quotesFilesListFiltered.length > 1) {

                    for (int i = 0; i < quotesFilesListFiltered.length; i++) {

                        System.out.print((i + 1) + ". " + quotesFilesListFiltered[i].getName().substring(0, quotesFilesListFiltered[i].getName().length() - 4) + "\n");

                    }

                    System.out.println();

                    while (!filesLoaded) {

                        try {

                            System.out.print("Introduce the number of the quotes file that you want to load: ");
                            quotesFile = quotesFilesListFiltered[userValues.nextInt() - 1];
                            System.out.print("\nQuotes file " + quotesFile.getName().substring(0, quotesFile.getName().length() - 4) + " loaded.\n");
                            System.out.print(loadRecords(quotesFile) + " loaded records.\n\n");

                            filesLoaded = true;
                            userValues.nextLine();

                        } catch (InputMismatchException e) {

                            System.out.print("\nERROR: Invalid input data\n");
                            userValues.nextLine();

                        } catch (ArrayIndexOutOfBoundsException e) {

                            System.out.print("\nERROR: The number that you introduced doesn't belong to any file.\n");

                        }

                    }

                } else {

                    quotesFile = quotesFilesListFiltered[0];
                    System.out.print("Quotes file " + quotesFile.getName().substring(0, quotesFile.getName().length() - 4) + " loaded.\n");
                    System.out.print(loadRecords(quotesFile) + " loaded records.\n\n");

                    filesLoaded = true;

                }

            } else {

                System.out.print("ERROR: There isn't any quotes file in the quotes file directory.\nPress Enter after introducing a quotes file in his directory (check for the directory structure in your user folder).\n");

                try {

                    System.in.read();

                } catch (Exception e) {

                }

            }

        }

    }

    /**
     * Loads all the records in its designed variable.
     *
     * @param loadedQuotesFile the quotes file that is reading.
     * @return the quantity of records that loaded.
     */
    public static int loadRecords(File loadedQuotesFile) {

        Scanner checkProcess = null;
        int recordsLength = 0;

        try {

            checkProcess = new Scanner(loadedQuotesFile);
            checkProcess.nextLine();

            while (checkProcess.hasNextLine()) {

                recordsLength++;
                checkProcess.nextLine();

            }

        } catch (Exception e) {

        }

        checkProcess.close();

        quotesRecords = new String[recordsLength][7];

        Scanner readingProcess = null;

        try {

            readingProcess = new Scanner(loadedQuotesFile);
            readingProcess.nextLine();

            for (int i = 0; i < quotesRecords.length; i++) {

                String[] splittedRecords = readingProcess.nextLine().split(",");

                for (int j = 0; j < quotesRecords[i].length; j++) {

                    quotesRecords[i][j] = splittedRecords[j];

                }

            }

        } catch (Exception e) {

        }

        readingProcess.close();

        return recordsLength;

    }

    /**
     * Gets the command that the user wants to execute. It will also display the
     * wallet that is currently loaded.
     *
     * @return the command that the player wants to execute.
     */
    public static String commandPrompt() {

        if (walletFile == null) {

            System.out.print("> ");

        } else {

            System.out.print(walletFile.getName().substring(0, walletFile.getName().length() - 4) + " > ");

        }

        return userValues.nextLine();

    }

    /**
     * Displays the value of the introduced field at the introduced date.
     *
     * @param date the date that the user wants to check
     * @param field the field that the user wants to check
     * @return returns the value of the field at the introduced date. If the
     * date or the field is invalid, it will return an error.
     */
    public static String checkValue(String date, String field) {

        int datePosition = checkDatePosition(date);

        if (datePosition == -1) {

            return "ERROR: The date introduced is invalid. Check if you introduced it in the correct format (yyyy-mm-dd) or if the date is registered.";

        } else {

            switch (field) {

                case "open":
                    return quotesRecords[datePosition][1];
                case "high":
                    return quotesRecords[datePosition][2];
                case "low":
                    return quotesRecords[datePosition][3];
                case "close":
                    return quotesRecords[datePosition][4];
                case "volume":
                    return quotesRecords[datePosition][6];
                default:
                    return "ERROR: The field introduced is invalid. The available fields are \"open\", \"high\", \"low\", \"close\" and \"volume\".";

            }

        }

    }

    /**
     * Displays the arithmetic average of the values of the introduced field
     * that are between the introduced dates.
     *
     * @param initialDate the first date where the program will get the first
     * value.
     * @param finalDate the last date where the program will get the last value.
     * @param field the field that the user wants to check
     * @return returns the arithmetic average. If the date or the field is
     * invalid, it will return an error.
     */
    public static String checkAverage(String initialDate, String finalDate, String field) {

        int initialDatePosition = checkDatePosition(initialDate);
        int finalDatePosition = checkFinalDatePosition(initialDatePosition, finalDate);

        if (initialDatePosition == -1) {

            return "ERROR: The initial date introduced is invalid. Check if you introduced it in the correct format (yyyy-mm-dd) or if the date is registered.";

        } else if (finalDatePosition == -1) {

            return "ERROR: The final date introduced is invalid. Check if you introduced it in the correct format (yyyy-mm-dd), if the date is registered or if you introduced the dates in the correct order.";

        } else {

            switch (field) {

                case "open":
                    return Double.toString(calculateAvg(initialDatePosition, finalDatePosition, 1));
                case "high":
                    return Double.toString(calculateAvg(initialDatePosition, finalDatePosition, 2));
                case "low":
                    return Double.toString(calculateAvg(initialDatePosition, finalDatePosition, 3));
                case "close":
                    return Double.toString(calculateAvg(initialDatePosition, finalDatePosition, 4));
                case "volume":
                    return Double.toString(calculateAvg(initialDatePosition, finalDatePosition, 6));
                default:
                    return "ERROR: The field introduced is invalid. The available fields are \"open\", \"high\", \"low\", \"close\" and \"volume\".";

            }

        }

    }

    /**
     * Calculates the arithmetic average of all the values that will be stored
     *
     * @param initialPosition the first position where the program will get the
     * first value
     * @param finalPosition the last position where the program will get the
     * last value
     * @param field the field that the user wants to check
     * @return the final result after performing the calculation
     */
    public static double calculateAvg(int initialPosition, int finalPosition, int field) {

        int numOfValues = finalPosition - initialPosition + 1;
        double finalValue = 0.0;

        for (int i = initialPosition; i <= finalPosition; i++) {

            finalValue += Double.parseDouble(quotesRecords[i][field]);

        }

        finalValue = finalValue / numOfValues;

        return finalValue;

    }

    /**
     * Displays the rentability percentage between a buying date and a selling
     * date.
     *
     * @param initialDate the first date where the program will get the first
     * value.
     * @param finalDate the last date where the program will get the last value.
     * @return returns the rentability percentage with 2 decimals max.
     */
    public static String calculateRent(String initialDate, String finalDate) {

        int initialDatePosition = checkDatePosition(initialDate);
        int finalDatePosition = checkFinalDatePosition(initialDatePosition, finalDate);

        if (initialDatePosition == -1) {

            return "ERROR: The initial date introduced is invalid. Check if you introduced it in the correct format (yyyy-mm-dd) or if the date is registered.";

        } else if (finalDatePosition == -1) {

            return "ERROR: The final date introduced is invalid. Check if you introduced it in the correct format (yyyy-mm-dd), if the date is registered or if you introduced the dates in the correct order.";

        } else {

            double rentability = (Double.parseDouble(quotesRecords[finalDatePosition][4]) - Double.parseDouble(quotesRecords[initialDatePosition][1])) / Double.parseDouble(quotesRecords[initialDatePosition][1]) * 100;

            return String.format("%.2f", rentability) + "%";

        }

    }

    /**
     * Displays the percentage of monkeys that would have rentability when
     * buying and selling bitcoins.
     *
     * @param monkeyAmount the amount of monkeys that will be used in the test.
     * @return the percentage of monkeys that got rentability.
     */
    public static String applyMonkeyTheory(int monkeyAmount) {

        System.out.print("\nCalculating the percentage of winning monkeys...\n");
        System.out.print("Remember that the calculation process will take longer the more monkeys you have introduced.\n");

        double winnerMonkeys = 0;

        for (int i = 0; i < monkeyAmount; i++) {

            int initialDatePos = (int) (Math.random() * quotesRecords.length - 1);
            int finalDatePos = (int) (Math.random() * (quotesRecords.length - initialDatePos) + initialDatePos);
            String monkeyRent = calculateRent(quotesRecords[initialDatePos][0], quotesRecords[finalDatePos][0]);
            monkeyRent = monkeyRent.replace(",", ".");

            if (Double.parseDouble(monkeyRent.substring(0, monkeyRent.length() - 1)) > 0.0) {

                winnerMonkeys++;

            }

        }

        return String.format("%.1f", (winnerMonkeys / monkeyAmount) * 100) + "%";

    }

    /**
     * Checks the position of the date that the user has introduced.
     *
     * @param date the date that is going to be checked to get its position.
     * @return the position of the date.
     */
    public static int checkDatePosition(String date) {

        int datePosition = -1;
        int positionCounter = 0;

        do {

            if (date.equals(quotesRecords[positionCounter][0])) {

                datePosition = positionCounter;
                positionCounter = quotesRecords.length;

            } else {

                positionCounter++;

            }

        } while (positionCounter < quotesRecords.length);

        return datePosition;

    }

    /**
     * Checks the position of the second date that the user has introduced.
     *
     * @param initialDatePosition the position of the first date.
     * @param date the date that is going to be checked to get its position.
     * @return the position of the date.
     */
    public static int checkFinalDatePosition(int initialDatePosition, String date) {

        int datePosition = -1;
        int positionCounter = initialDatePosition;

        do {

            if (date.equals(quotesRecords[positionCounter][0])) {

                datePosition = positionCounter;
                positionCounter = quotesRecords.length;

            } else {

                positionCounter++;

            }

        } while (positionCounter < quotesRecords.length);

        return datePosition;

    }

    /**
     * Lists all the wallets that are available. If there isn't any available,
     * it will display a error message.
     */
    public static void listWallets() {

        File[] walletsListUnfiltered = null;

        walletsListUnfiltered = walletsDir.listFiles();
        int filter = 0;

        for (int i = 0; i < walletsListUnfiltered.length; i++) {

            if (walletsListUnfiltered[i].getName().contains(".csv")) {

                filter++;

            }

        }

        File[] walletsListFiltered = new File[filter];
        filter = 0;

        for (int i = 0; i < walletsListUnfiltered.length; i++) {

            if (walletsListUnfiltered[i].getName().contains(".csv")) {

                walletsListFiltered[filter] = walletsListUnfiltered[i];
                filter++;

            }

        }

        if (walletsListFiltered.length > 0) {

            System.out.println();

            for (int i = 0; i < walletsListFiltered.length; i++) {

                System.out.print((i + 1) + ". " + walletsListFiltered[i].getName().substring(0, walletsListFiltered[i].getName().length() - 4) + "\n");

            }

            System.out.println();

        } else {

            System.out.print("\nERROR: There isn't any wallet available.\n\n");

        }

    }

    /**
     * Creates a new wallet and selects it.
     *
     * @param walletName the name of the wallet
     * @return a confirmation about the creation of the wallet or a error
     * message if the creation failed.
     */
    public static String newWallet(String walletName) {

        File newWallet = new File(walletsDir.getAbsolutePath() + File.separator + walletName + ".csv");

        if (newWallet.exists()) {

            return "ERROR: " + walletName + " already exists. Select it using the \"select\" command.";

        } else {

            try {

                newWallet.createNewFile();
                walletFile = newWallet;

            } catch (Exception e) {

                return "ERROR: Unexpected error while creating the new wallet.";

            }

            addDataToWallet("DATE", "TYPE OF TRANSACTION", "USD SPENT/EARNED", "TOTAL AMOUNT OF BTC");
            loadWalletValues();
            return walletName + " created and selected succesfully.";

        }

    }

    /**
     * Selects the wallet that the user specified. If it doesn't exist, a error
     * message will be displayed.
     *
     * @param walletName the name of the wallet.
     * @return a confirmation if the wallet was selected or a error message.
     */
    public static String selectWallet(String walletName) {

        String walletNameExtension = walletName + ".csv";
        File[] walletList = walletsDir.listFiles();

        for (int i = 0; i < walletList.length; i++) {

            if (walletNameExtension.equals(walletList[i].getName().toLowerCase())) {

                walletFile = walletList[i];
                loadWalletValues();
                return walletFile.getName().substring(0, walletFile.getName().length() - 4) + " selected.";

            }

        }

        return "ERROR: The wallet that you specified doesn't exist. Create it using the \"new\" command.";

    }

    /**
     * Writes a new line with the data of the transaction.
     *
     * @param firstValue the first value, usually the date of the transaction.
     * @param secondValue the second value, usually the type of transaction.
     * @param thirdValue the third value, usually the USD that were spent/earned
     * at the transaction.
     * @param fourthValue the fourth value, usually the total amount of BTC that
     * the wallet has.
     */
    public static void addDataToWallet(String firstValue, String secondValue, String thirdValue, String fourthValue) {

        PrintStream writingProcess = null;

        try {

            FileOutputStream walletLocation = new FileOutputStream(walletFile, true);
            writingProcess = new PrintStream(walletLocation);
            writingProcess.println(firstValue + "," + secondValue + "," + thirdValue + "," + fourthValue);

        } catch (Exception e) {

            System.out.print("\nERROR: Unexpected error while writing new data at the wallet.\n\n");

        }

    }

    /**
     * Loads the data that the program needs to work properly.
     */
    public static void loadWalletValues() {

        Scanner readingProcess = null;

        try {

            readingProcess = new Scanner(walletFile);
            readingProcess.nextLine();

            if (readingProcess.hasNextLine()) {

                String[] splittedData = null;

                while (readingProcess.hasNextLine()) {

                    splittedData = readingProcess.nextLine().split(",");

                }

                walletLastDatePosition = checkDatePosition(splittedData[0]);
                totalBTCAmount = Double.valueOf(splittedData[3]);

            } else {

                walletLastDatePosition = 0;
                totalBTCAmount = 0.0;

            }

        } catch (Exception e) {

            System.out.print("\nERROR: Unexpected error while loading the data of the wallet.\n\n");

        }

    }

    /**
     * Creates a purchase transaction at the selected wallet, using the
     * introduced date and USD amount to check how many BTC will own the user.
     *
     * @param date the date from which the opening value will be extracted.
     * @param usdAmount the USD amount that the user is spending in the
     * transaction.
     * @return this could return a confirmation message that the transaction was
     * successful or a error message.
     */
    public static String buyBTC(String date, double usdAmount) {

        int transactionDate = checkFinalDatePosition(walletLastDatePosition, date);

        if (transactionDate == -1) {

            return "ERROR: The introduced date is invalid. Check if it has the correct format or if it's registered in the quotes file.\nRemember that if you introduced a date that is older than the date of the last transaction it will make the introduced date invalid,\nsince you can't \"go to the past\" to create a new transaction.";

        }
        if (usdAmount <= 0) {

            return "ERROR: You can't buy bitcoins with 0 USD or less than 0 USD.";

        }

        addDataToWallet(quotesRecords[transactionDate][0], "BUY TRANSACTION", String.format(Locale.US, "%.6f", usdAmount), String.format(Locale.US, "%.6f", totalBTCAmount + usdAmount / Double.parseDouble(quotesRecords[transactionDate][1])));
        loadWalletValues();
        return "The purchase transaction has been registered (" + String.format(Locale.US, "%.6f", usdAmount / Double.parseDouble(quotesRecords[transactionDate][1])) + " BTC).";

    }

    /**
     * Creates a sale transaction at the selected wallet, using the introduced
     * date and BTC amount to check how many USD will earn the user.
     *
     * @param date the date from which the opening value will be extracted.
     * @param btcAmount the BTC amount that the user is spending in the
     * transaction.
     * @return this could return a confirmation message that the transaction was
     * successful or a error message.
     */
    public static String sellBTC(String date, double btcAmount) {

        int transactionDate = checkFinalDatePosition(walletLastDatePosition, date);

        if (transactionDate == -1) {

            return "ERROR: The introduced date is invalid. Check if it has the correct format or if it's registered in the quotes file.\nRemember that if you introduced a date that is older than the date of the last transaction it will make the introduced date invalid,\nsince you can't \"go to the past\" to create a new transaction.";

        }
        if (btcAmount > totalBTCAmount) {

            return "ERROR: You don't own the amount of bitcoin that you introduced.";

        }

        addDataToWallet(quotesRecords[transactionDate][0], "SELL TRANSACTION", String.format(Locale.US, "%.6f", btcAmount * Double.parseDouble(quotesRecords[transactionDate][4])), String.format(Locale.US, "%.6f", totalBTCAmount - btcAmount));
        loadWalletValues();
        return "The sale transaction has been registered (" + String.format(Locale.US, "%.6f", btcAmount * Double.parseDouble(quotesRecords[transactionDate][4])) + " USD).";

    }

    /**
     * Displays the balance of the wallet and other type of information (all USD
     * spent/earned, the rentability of the wallet...).
     */
    public static void walletBalance() {

        double spentQuantity = checkWalletValue("BUY TRANSACTION");
        double earnedQuantity = checkWalletValue("SELL TRANSACTION");

        if (spentQuantity == -1 || earnedQuantity == -1) {

            System.out.print("ERROR: A unexpected error has occurred while trying to read the wallet file. The action will be aborted.\n\n");
            return;

        }

        System.out.print("\nDisplaying the balance of the wallet " + walletFile.getName().substring(0, walletFile.getName().length() - 4) + "\n\n");
        System.out.print("Amount of USD spent on purchase transactions: " + spentQuantity + " USD\n");
        System.out.print("Amount of USD earned on purchase transactions: " + earnedQuantity + " USD\n");
        System.out.print("BTC valoration: " + totalBTCAmount + " BTC\n");

        if (earnedQuantity - spentQuantity < 0) {

            System.out.print("You have not been able to recover all the USD invested and you have lost " + ((earnedQuantity - spentQuantity) * - 1) + " USD (" + String.format("%.2f", (earnedQuantity - spentQuantity) / spentQuantity * 100) + "% of rentability)\n");
        } else if (earnedQuantity - spentQuantity > 0) {

            System.out.print("You have been able to recover all your invested USD and you have earned " + (earnedQuantity - spentQuantity) + " extra USD (" + String.format("%.2f", (earnedQuantity - spentQuantity) / spentQuantity * 100) + "% of rentability)\n");

        } else {

            System.out.print("You have been able to recover all your invested USD but you have not been able to earn any extra USD (0% of rentability)\n");

        }

        System.out.print("You would earn " + (totalBTCAmount * Double.parseDouble(quotesRecords[quotesRecords.length - 1][4])) + " USD if you sold your available BTC on the last recorded date\n\n");

    }

    /**
     * Checks the USD spent/earned depending on the transaction type and sums
     * it.
     *
     * @param transactionType the type of transaction that the function will
     * check.
     * @return the total amount of USD spent/earned.
     */
    public static double checkWalletValue(String transactionType) {

        Scanner readingProcess = null;
        double totalValue = 0;

        try {

            readingProcess = new Scanner(walletFile);
            readingProcess.nextLine();

            while (readingProcess.hasNextLine()) {

                String[] walletValues = readingProcess.nextLine().split(",");

                if (walletValues[1].equals(transactionType)) {

                    totalValue += Double.parseDouble(walletValues[2]);

                }

            }

        } catch (Exception e) {

            System.out.print("\nERROR: Unexpected error while trying to read the wallet file.\n");
            return -1;

        }

        return totalValue;

    }

}

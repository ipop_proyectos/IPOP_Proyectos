/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package minesweeper;

import java.util.*;
import java.io.*;

/**
 * This program is a version of the game "Minesweeper", a game that consists in clearing a minefield without triggering any mine. <br>
 * When starting the program, a main menu will load. There the player can: <br> <br>
 * 
 * - Start a new game <br>
 * - Loads a game that has been saved previously <br>
 * - Check the game history <br>
 * - Exit the game <br> <br>
 * 
 * 1. Starting a new game <br> <br>
 *
 *      The program will ask the user the length of the minefield and the percentage of mines that will be present in the minefield. <br>
 *      Then it will print the minefield at the start of the game and after performing an action. <br>
 *      After printing the minefield, the user will be able to perform an action: <br> <br>
 * 
 *          - "help" - Displays a menu that tells the user what actions he can perform. <br>
 *          - "save" - Will save the game after asking the user for a name for the file. <br>
 *          - "end" - Closes the current game. <br>
 *          - "cheat" - Shows the location of the mines. <br>
 *          - "normal" - Hides the location of the mines. <br>
 *          - "XX-YY" - Reveals the desired square in the minefield. "XX" equals to the rows and "YY" equals to the columns. <br> <br>
 * 
 *      The game will end whether the player wins or loses and will ask for a username to register the game in the game history. <br> <br>
 * 
 *  2. Loading a existing game: <br> <br>
 * 
 *      The program will list the save files that are available, listing the newer ones first, and will allow to pick a save file based on its number. If the save file was modified by the user, the program may be not able to load the desired game. Modifying the length of the table will display that the field was modified. <br>
 *      Then, the program will delete the save file and will load the game in the state that was left when saved. <br> <br>
 * 
 *  3. Check the game history: <br> <br>
 * 
 *      The program will display all the relevant information of the games that the player had played. It will display the username introduced by the user, the length of the minefield, the quantity of the mines, if he won or he lost and the date of the game. <br> <br>
 * 
 *  4. Exit the game: <br> <br>
 * 
 *      The program will ask for confirmation to the user. If the user answers "yes", the program will close.
 *
 * @author David Ruiz Agustín
 */
public class MineSweeper {

    /**
     * Defines the symbol that a square will display if its content hasn't been
     * revealed yet
     */
    public static final char squareSymbol = '?';
    /**
     * Defines the symbol that a square will display if it contains a mine
     */
    public static final char mineSymbol = '*';
    /**
     * Array that contains information about the location of the mines
     */
    public static boolean[][] mineField = new boolean[1][1];
    /**
     * Array that contains the information about the state of all the squares
     */
    public static int[][] mineFieldState = new int[1][1];
    /**
     * Initializes the variable that defines how many squares you need to reveal
     * to be able to win
     */
    public static int squaresToWin = 0;
    /**
     * This variable defines the location where all the program files will be located
     */
    public static File baseFolder = new File(System.getenv("LOCALAPPDATA") + File.separator + "MineSweeper");
    /**
     * This variable defines the location where all the save files will be saved
     */
    public static File saveFolder = new File(baseFolder.getAbsolutePath() + File.separator + "savefiles");
    /**
     * This variable defines the location of the game history file
     */
    public static File gameHistory = new File(baseFolder.getAbsolutePath() + File.separator + "gamehistory.bmi");

    /**
     * Starting point
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //Creates all the files that are needed to save the games and game history if they weren't created
        try {

            baseFolder.mkdir();
            saveFolder.mkdir();
            gameHistory.createNewFile();

        } catch (Exception e) {
            System.out.print("ERROR: Unexpected error occurred while creating saving files\n\n");
        }

        boolean wantToExit = false;

        //Loads the main menu and will perform the action requested by the user
        do {

            int option = mainMenu();

            switch (option) {

                case 1:
                    int fieldSize = defineFieldSize();
                    float minePercentage = defineMinePercentage();
                    generateMineField(fieldSize, minePercentage);
                    squaresToWin = 0;
                    playGame(fieldSize, minePercentage);
                    break;
                case 2:
                    boolean canLoadGame = loadGame();
                    if (canLoadGame) {
                        playGame(0, 0);
                    }
                    break;
                case 3:
                    checkGameHistory();
                    break;
                case 4:
                    wantToExit = exitGame();
                    break;

            }

            System.out.println();

        } while (!wantToExit);

    }

    /**
     * Module that displays the main menu of the program
     *
     * @return the option that the player has chosen
     */
    public static int mainMenu() {

        Scanner userValues = new Scanner(System.in);

        int option = 0;

        System.out.print("-------------------------------------------\n\n");
        System.out.print("Welcome to \"Minesweeper\"\n");
        System.out.print("Select what option do you wish to perform\n\n");
        System.out.print("-------------------------------------------\n\n");
        System.out.print("1. Start a new game\n");
        System.out.print("2. Load a game\n");
        System.out.print("3. Show the game history\n");
        System.out.print("4. Exit game\n");

        do {

            try {

                System.out.print("\nOption: ");
                option = userValues.nextInt();

                switch (option) {

                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        return option;
                    default:
                        System.out.println("ERROR: Invalid option.");
                        break;

                }

            } catch (Exception e) {
                System.out.print("\nERROR: Invalid data type");
                userValues.nextLine();
                option = 0;
            }

        } while (option < 1 || option > 4);

        return 0;

    }

    /**
     * Module that gives the option to decide the size of the minefield
     *
     * @return the length of the minefield
     */
    public static int defineFieldSize() {

        Scanner userValues = new Scanner(System.in);
        int option;

        System.out.print("\nSelect one of the following sizes:\n\n");
        System.out.print("1. Small (4x4)\n");
        System.out.print("2. Medium (8x8)\n");
        System.out.print("3. Large (12x12)\n");

        do {

            try {

                System.out.print("\nOption: ");
                option = userValues.nextInt();

                switch (option) {

                    case 1:
                        return 4;
                    case 2:
                        return 8;
                    case 3:
                        return 12;
                    default:
                        System.out.print("\nERROR: Invalid option\n");

                }

            } catch (Exception e) {
                System.out.print("\nERROR: Invalid data type");
                userValues.nextLine();
                option = 0;
            }

        } while (option < 1 || option > 3);

        return 0;

    }

    /**
     * Module that gives the option to decide the quantity of mines in the field
     *
     * @return the percentage of mines in the field
     */
    public static float defineMinePercentage() {

        Scanner userValues = new Scanner(System.in);
        int option;

        System.out.print("\nSelect the quantity of mines:\n\n");
        System.out.print("1. Low quantity (20% of mines)\n");
        System.out.print("2. Average quantity (30% of mines)\n");
        System.out.print("3. High quantity (40% of mines)\n");

        do {

            try {

                System.out.print("\nOption: ");
                option = userValues.nextInt();

                switch (option) {

                    case 1:
                        return 0.2f;
                    case 2:
                        return 0.3f;
                    case 3:
                        return 0.4f;
                    default:
                        System.out.print("\nERROR: Invalid option\n");

                }

            } catch (Exception e) {
                System.out.print("\nERROR: Invalid data type");
                userValues.nextLine();
                option = 0;
            }

        } while (option < 1 || option > 3);

        return 0.0f;

    }

    /**
     * Module that generates the mine field with its mines
     *
     * @param fieldSize the size of the minefield
     * @param minePercentage the percentage of the mines in the field
     */
    public static void generateMineField(int fieldSize, float minePercentage) {

        mineField = new boolean[fieldSize][fieldSize];
        mineFieldState = new int[fieldSize][fieldSize];

        int mineQuantity = (int) ((fieldSize * fieldSize) * minePercentage);

        //This loop will generate all the mines according to the number of squares and the mine percentage
        while (mineQuantity > 0) {

            int mineFieldRow = (int) (Math.random() * fieldSize);
            int mineFieldColumn = (int) (Math.random() * fieldSize);

            if (mineField[mineFieldRow][mineFieldColumn] == false) {

                mineField[mineFieldRow][mineFieldColumn] = true;
                mineQuantity--;

            }

        }

    }

    /**
     * Module that allows the player to play the minesweeper game
     *
     * @param fieldSize the size of the minefield
     * @param minePercentage the percentage of the mines in the field
     */
    public static void playGame(int fieldSize, float minePercentage) {

        Scanner userValues = new Scanner(System.in);
        boolean quitGame = false;

        //Variables that are used to determine if the player won or lost the game. Some of them will be calculated when starting a new game
        if (squaresToWin == 0) {
            int totalSquares = fieldSize * fieldSize;
            squaresToWin = totalSquares - (int) (totalSquares * minePercentage);
        }
        boolean playerLost = false;

        //This loop will print the upper row that shows the number of the columns
        do {

            System.out.println();
            System.out.print("   ");

            for (int i = 0; i < mineField.length; i++) {

                if (i < 9) {
                    System.out.print(" " + (i + 1) + " ");
                } else {
                    System.out.print((i + 1) + " ");
                }

            }

            System.out.print("\n");

            //Loop that will print the mine field for every turn that the player plays
            for (int i = 0; i < mineField.length; i++) {

                if (i < 9) {
                    System.out.print(" " + (i + 1) + " ");
                } else {
                    System.out.print((i + 1) + " ");
                }

                for (int j = 0; j < mineField[i].length; j++) {

                    switch (mineFieldState[i][j]) {

                        case 0:
                            System.out.print(" " + squareSymbol + " ");
                            break;
                        case 1:
                            int minesAround = checkMines(i, j);

                            if (minesAround == 0) {
                                System.out.print("   ");
                            } else {
                                System.out.print(" " + minesAround + " ");
                            }

                            break;
                        case 2:
                            System.out.print(" " + mineSymbol + " ");
                            break;

                    }

                }

                System.out.print("\n");

            }

            System.out.print("\nIntroduce the action that you want to perform (\"help\" for more info): ");

            String playerElection = userValues.nextLine();

            boolean validAction = false;

            //This loop will let the player perform one of the allowed actions. If he introduces any action that isn't available, it will ask the user to introduce another action
            while (!validAction) {

                switch (playerElection.toLowerCase()) {

                    //Displays a small help menu with the allowed actions
                    case "help":
                        System.out.print("\nSAVE\t\tSaves the current game with the current data.");
                        System.out.print("\nEND\t\tQuits the game without saving.");
                        System.out.print("\nCHEAT\t\tReveals the location of every mine in the minefield.");
                        System.out.print("\nNORMAL\t\tHides the location of every mine in the minefield.");
                        System.out.print("\nX-Y\t\tReveals the content of the square based on the location given. X equals the row and Y equals the column.\n");
                        validAction = true;
                        break;
                    //The user will save the current mine locations, the minefield state, the length and the remaining undiscovered squares
                    case "save":
                        saveGame();
                        validAction = true;
                        break;
                    //The user will close the current game
                    case "end":
                        System.out.print("\nAre you sure that you want to exit this game? All data will be lost: ");
                        playerElection = userValues.nextLine();

                        do {

                            switch (playerElection.toLowerCase()) {

                                case "no":
                                case "n":
                                    validAction = true;
                                    break;
                                case "yes":
                                case "y":
                                    return;
                                default:
                                    System.out.print("\nERROR: Invalid answer. Please state if you want to exit the current game: ");
                                    playerElection = userValues.nextLine();

                            }

                        } while (!validAction);
                        break;
                    //The player will be able to see the location of the mines. What a sissy.
                    case "cheat":
                        for (int i = 0; i < mineField.length; i++) {

                            for (int j = 0; j < mineField[i].length; j++) {

                                if (mineField[i][j] == true) {
                                    mineFieldState[i][j] = 2;
                                }

                            }

                        }
                        validAction = true;
                        break;
                    //The player will hide the location of the mines
                    case "normal":
                        for (int i = 0; i < mineField.length; i++) {

                            for (int j = 0; j < mineField[i].length; j++) {

                                if (mineField[i][j] == true) {
                                    mineFieldState[i][j] = 0;
                                }

                            }

                        }
                        validAction = true;
                        break;
                    //The player will discover the desired location. If it has a mine, he will lose the game.
                    default:

                        if ((playerElection.length() < 6 || playerElection.length() > 2) && playerElection.contains("-")) {

                            try {

                                String[] splittedCoords = playerElection.split("-");
                                int xCoords = Integer.parseInt(splittedCoords[0]) - 1;
                                int yCoords = Integer.parseInt(splittedCoords[1]) - 1;

                                if ((xCoords > -1 && xCoords < mineFieldState.length) && (yCoords > -1 && yCoords < mineFieldState.length)) {

                                    if (mineFieldState[xCoords][yCoords] == 1) {

                                        System.out.print("\nERROR: Square already revealed");
                                        System.out.print("\nAction: ");
                                        playerElection = userValues.nextLine();

                                    } else {

                                        playerLost = revealSquare(xCoords, yCoords);
                                        validAction = true;
                                    }

                                } else {

                                    System.out.print("\nERROR: Invalid location");
                                    System.out.print("\nAction: ");
                                    playerElection = userValues.nextLine();

                                }

                            } catch (Exception e) {

                                System.out.print("\nERROR: Invalid data type");
                                System.out.print("\nAction: ");
                                playerElection = userValues.nextLine();

                            }

                        } else {

                            System.out.print("\nERROR: Invalid action. (Type \"help\" to get more information about the actions that you can perform)");
                            System.out.print("\nAction: ");
                            playerElection = userValues.nextLine();

                        }

                        break;

                }

            }

        } while (squaresToWin != 0 && !playerLost && !quitGame);

        //Condition that will reveal all the mines if the player had lost the game
        if (playerLost) {

            for (int i = 0; i < mineField.length; i++) {

                for (int j = 0; j < mineField[i].length; j++) {

                    if (mineField[i][j] == true) {
                        mineFieldState[i][j] = 2;
                    }

                }

            }

        }

        System.out.println();
        System.out.print("   ");

        for (int i = 0; i < mineField.length; i++) {

            if (i < 9) {
                System.out.print(" " + (i + 1) + " ");
            } else {
                System.out.print((i + 1) + " ");
            }

        }

        System.out.print("\n");

        //Loop showing the minefield whether the player wins or loses
        for (int i = 0; i < mineField.length; i++) {

            if (i < 9) {
                System.out.print(" " + (i + 1) + " ");
            } else {
                System.out.print((i + 1) + " ");
            }

            for (int j = 0; j < mineField[i].length; j++) {

                switch (mineFieldState[i][j]) {

                    case 0:
                        System.out.print(" " + squareSymbol + " ");
                        break;
                    case 1:
                        int minesAround = checkMines(i, j);

                        if (minesAround == 0) {
                            System.out.print("   ");
                        } else {
                            System.out.print(" " + minesAround + " ");
                        }

                        break;
                    case 2:
                        System.out.print(" " + mineSymbol + " ");
                        break;

                }

            }

            System.out.print("\n");

        }

        //This conditions will tell the player if he won or he lost and will ask for a name. When a text is given, it will save the current game in the game history
        if (playerLost == true) {

            System.out.print("\nYOU LOSE! Enter your name: ");
            String playerName = userValues.nextLine();
            PrintStream writingProcess = null;
            String fieldLength = null;
            switch (mineField.length) {
                case 4:
                    fieldLength = "Small field";
                    break;
                case 8:
                    fieldLength = "Medium field";
                    break;
                case 12:
                    fieldLength = "Large field";
                    break;
                default:
                    fieldLength = "MODIFIED FIELD";
                    break;
            }

            int minesInField = 0;

            for (int i = 0; i < mineField.length; i++) {

                for (int j = 0; j < mineField[i].length; j++) {

                    if (mineField[i][j] == true) {

                        minesInField++;

                    }

                }

            }

            try {

                FileOutputStream gameHistoryUbication = new FileOutputStream(gameHistory, true);
                writingProcess = new PrintStream(gameHistoryUbication);
                writingProcess.println(playerName + "\t\t" + fieldLength + "\t\t" + minesInField + " mines in the field\t\tLOSER\t\t" + java.time.LocalDateTime.now());

            } catch (Exception e) {

                System.out.print("\nERROR: Unexpected error while saving the game");

            }

        } else {

            System.out.print("\nYOU WON! Enter your name: ");
            String playerName = userValues.nextLine();
            PrintStream writingProcess = null;
            String fieldLength = null;
            switch (mineField.length) {
                case 4:
                    fieldLength = "Small field";
                    break;
                case 8:
                    fieldLength = "Medium field";
                    break;
                case 12:
                    fieldLength = "Large field";
                    break;
                default:
                    fieldLength = "MODIFIED FIELD";
                    break;
            }

            int minesInField = 0;

            for (int i = 0; i < mineField.length; i++) {

                for (int j = 0; j < mineField[i].length; j++) {

                    if (mineField[i][j] == true) {

                        minesInField++;

                    }

                }

            }

            try {

                FileOutputStream gameHistoryUbication = new FileOutputStream(gameHistory, true);
                writingProcess = new PrintStream(gameHistoryUbication);
                writingProcess.println(playerName + "\t\t" + fieldLength + "\t\t" + minesInField + " mines in the field\t\tWINNER\t\t" + java.time.LocalDateTime.now());

            } catch (Exception e) {

                System.out.print("\nERROR: Unexpected error while saving the game");

            }

        }

    }

    /**
     * Method that will handle all the process of saving the game.
     */
    public static void saveGame() {

        Scanner userValues = new Scanner(System.in);
        System.out.print("\nIntroduce the name for the save file (it can't contain whitespaces): ");
        String saveFileName = userValues.nextLine();
        String[] checkFileName = saveFileName.split(" ");

        if (checkFileName.length > 1) {

            while (checkFileName.length > 1) {

                System.out.print("\nERROR: The introduced name contains whitespaces. Introduce another name: ");
                saveFileName = userValues.nextLine();
                checkFileName = saveFileName.split(" ");

            }

        }

        File saveFile = new File(saveFolder.getAbsolutePath() + File.separator + saveFileName + ".bmi");
        PrintStream writingProcess = null;

        try {

            writingProcess = new PrintStream(saveFile);

            //Save minefield length
            writingProcess.print(mineField.length + "\n");

            //Save the position of the mines
            for (int i = 0; i < mineField.length; i++) {

                for (int j = 0; j < mineField[i].length; j++) {

                    if (j == mineField[i].length - 1) {

                        writingProcess.print(mineField[i][j] + "\n");

                    } else {

                        writingProcess.print(mineField[i][j] + " - ");

                    }

                }

            }

            //Save minefield status
            for (int i = 0; i < mineFieldState.length; i++) {

                for (int j = 0; j < mineFieldState[i].length; j++) {

                    if (j == mineField[i].length - 1) {

                        writingProcess.print(mineFieldState[i][j] + "\n");

                    } else {

                        writingProcess.print(mineFieldState[i][j] + " - ");

                    }

                }

            }

            //Save the number of squares needed to win the game
            writingProcess.print(squaresToWin);

        } catch (Exception e) {

            System.out.print("\nERROR: Unexpected error while saving the game");

        }

        writingProcess.close();

    }

    /**
     * Method that will handle all the process of loading a game
     * 
     * @return if the program will load a save file
     */
    public static boolean loadGame() {

        Scanner userValues = new Scanner(System.in);
        Scanner readingProcess = null;
        File[] saveFilesList = null;

        //Loads all the save files available
        try {

            saveFilesList = saveFolder.listFiles();

            if (saveFilesList.length > 0) {

                Arrays.sort(saveFilesList, Comparator.comparingLong(File::lastModified).reversed());

                String[] saveFilesName = new String[saveFilesList.length];

                System.out.println();

                for (int i = 0; i < saveFilesList.length; i++) {
                    saveFilesName[i] = saveFilesList[i].getName();
                    saveFilesName[i] = saveFilesName[i].substring(0, saveFilesName[i].length() - 4);
                    System.out.println((i + 1) + ". " + saveFilesName[i]);
                }

            } else {
                System.out.print("\nERROR: There isn't any save file available\n");
                return false;
            }

        } catch (Exception e) {
            System.out.print("\nERROR: Unexpected error while loading save files\n");
            return false;
        }

        boolean validInput = false;
        int saveFileNumber = 0;

        while (!validInput) {

            System.out.print("\nSelect the number of the save file that you wish to load: ");

            try {

                saveFileNumber = userValues.nextInt() - 1;

                if (saveFileNumber < 0 || saveFileNumber > saveFilesList.length - 1) {

                    System.out.print("\nERROR: Invalid save file number.");

                } else {
                    validInput = true;
                }

            } catch (Exception e) {

                System.out.print("\nERROR: Invalid data type");
                userValues.nextLine();

            }

        }

        File fileToLoad = new File(saveFolder.getAbsolutePath() + File.separator + saveFilesList[saveFileNumber].getName());

        //Loads the desired save file
        try {

            readingProcess = new Scanner(fileToLoad);

            //Loads the length of the mine field
            int loadedLength = readingProcess.nextInt();
            mineField = new boolean[loadedLength][loadedLength];
            mineFieldState = new int[loadedLength][loadedLength];
            readingProcess.nextLine();

            //Loads the location of each mine
            String mineFieldSquares = null;

            for (int i = 0; i < mineField.length; i++) {

                mineFieldSquares = readingProcess.nextLine();
                String[] splittedMines = mineFieldSquares.split(" - ");

                for (int j = 0; j < mineField[i].length; j++) {

                    mineField[i][j] = Boolean.parseBoolean(splittedMines[j]);

                }

            }

            //Loads the state of the minefield 
            for (int i = 0; i < mineFieldState.length; i++) {

                mineFieldSquares = readingProcess.nextLine();
                String[] splittedStates = mineFieldSquares.split(" - ");

                for (int j = 0; j < mineFieldState[i].length; j++) {

                    mineFieldState[i][j] = Integer.parseInt(splittedStates[j]);

                }

            }

            //Loads the quantity of squares left
            squaresToWin = readingProcess.nextInt();

        } catch (Exception e) {
            System.out.print("\nERROR: Unexpected error while loading the game\n");
            return false;
        }

        readingProcess.close();
        fileToLoad.delete();

        return true;

    }

    /**
     * Module that checks the surroundings of each discovered square
     *
     * @param i the x location of the square 
     * @param j the y location of the square
     * @return the number of mines that are around the revealed square
     */
    public static int checkMines(int i, int j) {

        int minesAround = 0;

        for (int x = i - 1; x < i + 2; x++) {

            if (x > -1 && x < mineField.length) {

                for (int y = j - 1; y < j + 2; y++) {

                    if (y > -1 && y < mineField.length) {

                        if (mineField[x][y] == true) {
                            minesAround++;
                        }

                    }

                }

            }

        }

        return minesAround;

    }

    /**
     * Method that will reveal the square selected. If the square selected isn't
     * surrounded by a mine, it will reveal the squares that surround him
     *
     * @param xCoords the x location that the player has chosen
     * @param yCoords the y location that the player has chosen
     * @return if the player has triggered a mine
     */
    public static boolean revealSquare(int xCoords, int yCoords) {

        if (mineField[xCoords][yCoords] == true) {

            return true;

        } else {

            mineFieldState[xCoords][yCoords] = 1;
            squaresToWin--;

            int minesAround = checkMines(xCoords, yCoords);

            if (minesAround == 0) {

                for (int i = xCoords - 1; i < xCoords + 2; i++) {

                    if (i > -1 && i < mineField.length) {

                        for (int j = yCoords - 1; j < yCoords + 2; j++) {

                            if (j > -1 && j < mineField.length) {

                                if (mineFieldState[i][j] == 0) {

                                    revealSquare(i, j);

                                }

                            }

                        }

                    }

                }

            }

        }

        return false;

    }

    /**
     * Method that will allow the player to check all the games that he played
     * in this program
     */
    public static void checkGameHistory() {

        Scanner readingProcess = null;

        try {

            readingProcess = new Scanner(gameHistory);

            System.out.println();
            if (readingProcess.hasNextLine()) {
                while (readingProcess.hasNextLine()) {

                    System.out.println(readingProcess.nextLine());

                }
            } else {

                System.out.println("The game history is empty.");

            }

            System.out.println();
            System.out.print("Press Enter to go back to the main menu...");

            try {

                System.in.read();

            } catch (Exception e) {

            }

        } catch (Exception e) {
            System.out.print("\nERROR: Unexpected error while loading game history\n");
            return;
        }

    }

    /**
     * Module that allows the player to exit the game
     *
     * @return if the player wants to end the program
     */
    public static boolean exitGame() {

        Scanner userValues = new Scanner(System.in);
        boolean validInput = false;

        do {

            System.out.print("\nAre you sure that you want to exit the game? ");
            String playerDecision = userValues.nextLine();

            switch (playerDecision.toLowerCase()) {

                case "no":
                case "n":
                    validInput = true;
                    return false;
                case "yes":
                case "y":
                    validInput = true;
                    return true;
                default:
                    System.out.println("ERROR: Invalid option");

            }

        } while (!validInput);

        return false;

    }

}
